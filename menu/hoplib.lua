WatchOutForEnemies = {}

WatchOutForEnemies.settings = {
    Chat = {
        Chat_Radio = 2,
        Chat_Cooldown = 10,
        Chat_Filter = {
            CF_Tag = {
                CFT_tank = true,
                CFT_taser = false,
                CFT_medic = false,
                CFT_spooc = false,
                CFT_shield = false,
                CFT_sniper = false
            }
        }
    },
    Highlight = {
        Highlight_Radio = true,
        Highlight_Persist = 5
    },
    Hud = {
        Hud_Switch = true,
        --[[Hud_Position = {
            Hud_X = 0,
            Hud_Y = 0,
            Hud_Z = 0
        },--]]
        Hud_Fading = true,
        Hud_Size = 3500,
        Hud_Color = {
            Hud_Color_a = {
                Hud_a_R = 107,
                Hud_a_G = 142,
                Hud_a_B = 35
            },
            Hud_Color_b = {
                Hud_b_R = 255,
                Hud_b_G = 0,
                Hud_b_B = 0
            }
        }
    },
    EnemySpotter = {
        ES_Enable = false,
        ES_Filter = {
            EST_taser = true,
            EST_medic = true,
            EST_spooc = true,
            EST_shield = true,
            EST_tank = true,
            EST_tank_medic = true,
            EST_tank_mini = true,
            EST_sniper = true,
			EST_phalanx_minion = true,
			EST_snowman_boss = true,
			EST_summers = true,
			EST_medic_summers = true,
			EST_taser_summers = true,
			EST_tank_titan = true,
			EST_taser_titan = true,
			EST_hrt_titan = true
        },
		ES_MaxDD = 15,
		ES_IconSize = 20,
		ES_FadeOut = true
    },
	FlashbangMarker = {
		FM_Enable = true,
		FM_Size = 20
	}
}

WatchOutForEnemies.values = {
    Chat = {
        priority = 100
    },
    Highlight = {
        priority = 55
    },
    Hud = {
        priority = 10
    },
    EnemySpotter = {
        priority = 8
    },
	FlashbangMarker = {
        priority = 7
    },

    CF_Tag = {
        priority = 100
    },

    Chat_Radio = {
        items = { "WOE_disable", "WOE_onlyme", "WOE_all" },
        priority = 100
    },
    Chat_Filter = {
        priority = 25
    },
    Chat_Cooldown = {
        min = 0,
        max = 20,
        step = 1,
        priority = 50
    },

    CFT_tank = {
        priority = 100
    },
    CFT_taser = {
        priority = 80
    },
    CFT_medic = {
        priority = 70
    },
    CFT_spooc = {
        priority = 50
    },
    CFT_shield = {
        priority = 90
    },
    CFT_sniper = {
        priority = 60
    },


    Hud_Switch = {
        priority = 100
    },
    Hud_Fading = {
        priority = 75
    },
    --[[Hud_Position = {
        priority = 55
    },--]]
    Hud_Size = {
        min = 1,
        max = 3500,
        step = 50,
        priority = 50
    },
    Hud_Color = {
        priority = 10
    },


    Hud_Color_a = {
        priority = 100
    },
    Hud_Color_b = {
        priority = 10
    },


    --[[Hud_X = {
        priority = 100
    },
    Hud_Y = {
        priority = 55
    },
    Hud_Z = {
        priority = 10
    },--]]

    Hud_a_R = {
        min = 1,
        max = 255,
        step = 1,
        priority = 100,
		callback = function()
            if HUDManager and HUDManager.bote_set_color then
			    HUDManager:bote_set_color()
            end
		end
	},
    Hud_a_G = {
        min = 1,
        max = 255,
        step = 1,
        priority = 55,
		callback = function()
            if HUDManager and HUDManager.bote_set_color then
			    HUDManager:bote_set_color()
            end
		end
    },
    Hud_a_B = {
        min = 1,
        max = 255,
        step = 1,
        priority = 10,
		callback = function()
            if HUDManager and HUDManager.bote_set_color then
			    HUDManager:bote_set_color()
            end
		end
    },

    Hud_b_R = {
        min = 1,
        max = 255,
        step = 1,
        priority = 100,
		callback = function()
            if HUDManager and HUDManager.bote_set_color then
			    HUDManager:bote_set_color()
            end
		end
    },
    Hud_b_G = {
        min = 1,
        max = 255,
        step = 1,
        priority = 55,
		callback = function()
            if HUDManager and HUDManager.bote_set_color then
			    HUDManager:bote_set_color()
            end
		end
    },
    Hud_b_B = {
        min = 1,
        max = 255,
        step = 1,
        priority = 10,
		callback = function()
            if HUDManager and HUDManager.bote_set_color then
			    HUDManager:bote_set_color()
            end
		end
    },


    Highlight_Radio = {
        priority = 75
    },
    Highlight_Persist = {
        min = 1,
        max = 10,
        step = 1,
        priority = 50
    },

    ES_Enable = {
        priority = 100
    },
    ES_Filter = {
        priority = 99
    },
    ES_MaxDD = {
        min = 1,
        max = 15,
        step = 1,
        priority = 98
    },
    ES_IconSize = {
        min = 5,
        max = 60,
        step = 1,
        priority = 97,
		callback = function()
            if EnemySpotter and EnemySpotter.set_size then
			    EnemySpotter:set_size()
            end
		end
    },
    ES_FadeOut = {
        priority = 96
    },
    EST_taser = {
        priority = 100
    },
    EST_medic = {
        priority = 99
    },
    EST_spooc = {
        priority = 98
    },
    EST_shield = {
        priority = 97
    },
    EST_tank = {
        priority = 96
    },
    EST_tank_medic = {
        priority = 95
    },
    EST_tank_mini = {
        priority = 94
    },
    EST_sniper = {
        priority = 93
    },
    EST_phalanx_minion = {
        priority = 92
    },
    EST_snowman_boss = {
        priority = 91
    },
    EST_summers = {
        priority = 90
    },
    EST_medic_summers = {
        priority = 89
    },
    EST_taser_summers = {
        priority = 88
    },
    EST_tank_titan = {
        priority = 87
    },
    EST_taser_titan = {
        priority = 86
    },
    EST_hrt_titan = {
        priority = 85
    },
	
	
	FM_Enable = {
		priority = 10
	},
	FM_Size = {
        min = 10,
        max = 50,
        step = 1,
        priority = 9
	}
}


local builder = MenuBuilder:new("watch_out_for_enemies", WatchOutForEnemies.settings, WatchOutForEnemies.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusWatchOutForEnemies", function(menu_manager, nodes)
  builder:create_menu(nodes)
end)

--localization
dofile(ModPath .. "menu/localization.lua")

