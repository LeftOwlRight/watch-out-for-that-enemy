if not WatchOutForEnemies then
	dofile(ModPath .. "menu/hoplib.lua")
end

WOE_Settings = WatchOutForEnemies.settings
WOE_Settings_Chat = WatchOutForEnemies.settings.Chat
WOE_Settings_Highlight = WatchOutForEnemies.settings.Highlight
WOE_Settings_Hud = WatchOutForEnemies.settings.Hud

--@param string
function WOE_Localization(string_id)
	if managers.localization then
		return managers.localization:text(string_id)
	else
		return ""
	end
end

--@param unit_id
--return unit, unit_base
function WOE_find_unit_by_id(Base_unit_id)
	for _, data in pairs(managers.enemy:all_enemies()) do
	    if data.unit:id() == Base_unit_id then
	        return data.unit, data.unit:base() or nil
	    end
	end
end

--get unit priority by unit's HP and disatance from me
function WOE_getHazardLevel(unit_health, unit_distance)
	local level = 0
	local player_health_ratio = managers.player:player_unit():character_damage():health_ratio()
	local player_armor_ratio = managers.player:player_unit():character_damage():armor_ratio()

	if player_armor_ratio < 0.5 then
		level = level +1
	end
	if player_armor_ratio < 0.3 then
		level = level +1
	end
	if player_health_ratio < 0.5 then
		level = level +1
	end
	if player_health_ratio < 0.3 then
		level = level +1
	end
	if unit_health < 1000 and unit_health > 400 then
		level = level +1
	end
	if unit_health > 3000 then
		level = level +1
	end
	if unit_health > 10000 then
		level = level +1
	end
	if unit_health > 20000 then
		level = level +1
	end
	if unit_distance <10 then
		level = level +1
	end
	if unit_distance <3 then
		level = level +1
	end
	return level
end


--LuaNetworking Receiver
Hooks:Add("NetworkReceivedData", "NetworkReceivedData_bote", function(sender, id, data)
	local peer_hook = managers.network and managers.network:session() or nil
	peer_hook = peer_hook and peer_hook:local_peer() or nil

	-- @param string  channel_id?
	local function numbers_get_from_data(func_id)
		local table_get_from_data = json.decode(data)
		--managers.chat:_receive_message(sender, func_id, data, tweak_data.system_chat_color)
		local func_number_data_A = 3
		local func_number_data_B = 1
		if table_get_from_data and table_get_from_data.value_1 then
			func_number_data_A = table_get_from_data.value_1
		end
		if table_get_from_data and table_get_from_data.value_2 then
			func_number_data_B = table_get_from_data.value_2
		end
		return func_number_data_A, func_number_data_B
	end

	--Get Cooldown time from others
	if id == "WOE_bote_copytime" then
		--number_data is Cooldown time
		local number_data, copytime_unit_id = numbers_get_from_data("WOE_bote_copytime")
		local copytime_unit, copytime_unit_base
		if copytime_unit_id then
			copytime_unit, copytime_unit_base = WOE_find_unit_by_id(copytime_unit_id)
		end
		if number_data and number_data ~= 0 then
			if copytime_unit and copytime_unit_base then
				copytime_unit_base.copy_time = number_data
			end
		end
	end

	--Get Highlight time from others
	if id == "WOE_bote_highlight_time" then
		--number_data is Highlight time
		local number_data, highlight_time_unit_id = numbers_get_from_data("WOE_bote_copytime")
		local highlight_time_unit, highlight_time_unit_base
		if highlight_time_unit_id then
			highlight_time_unit, highlight_time_unit_base = WOE_find_unit_by_id(highlight_time_unit_id)
		end
		if number_data then
			if highlight_time_unit and highlight_time_unit_base then
				highlight_time_unit_base.highlight_time = number_data
			end
		end
	end

	--Get who others mark (unit_id)
	if id == "WOE_bote_chatsend" then
		--number_data is unit_id
		local number_data = numbers_get_from_data("WOE_bote_chatsend")
		local chatsend_unit, chatsend_unit_base
		if number_data then
			chatsend_unit, chatsend_unit_base = WOE_find_unit_by_id(number_data)
		end

		--after knowing who got marked
		local copy_time = WOE_Settings_Chat.Chat_Cooldown  --in case we don't receive cooldown time from others
		local highlight_time = WOE_Settings_Highlight.Highlight_Persist  --in case we don't receive highlight time from others
		if chatsend_unit and chatsend_unit_base then

			--We say "copy" during cooldown time
			local function remove_copy()
				if not alive(chatsend_unit) or not chatsend_unit_base then
					return
				end
				chatsend_unit_base.is_noted_by_others = nil
				chatsend_unit_base.has_copy = nil
				chatsend_unit_base.net_delay_remove_copy = nil
			end
			copy_time = chatsend_unit_base.copy_time or copy_time
			chatsend_unit_base.is_noted_by_others = true  --so we will say "copy" to this unit
			if not chatsend_unit_base.net_delay_remove_copy then
				DelayedCalls:Add("WOE_remove_copy" .. number_data, copy_time, remove_copy)  --time is up, we don't "copy" it anymore
				chatsend_unit_base.net_delay_remove_copy = true
			end

			--avoid a unit gets highlighted multiple times
			local function remove_contour()
				if not alive(chatsend_unit) or not chatsend_unit_base then
					return
				end
				chatsend_unit:contour():remove("highlight_character", false, 1)
				chatsend_unit_base.is_contoured = 1
				chatsend_unit_base.multi_contoured_set = nil
				chatsend_unit_base.net_delay_remove_con = nil
			end
			highlight_time = chatsend_unit_base.highlight_time or highlight_time
			if not chatsend_unit_base.is_contoured or chatsend_unit_base.is_contoured == 1 then
				chatsend_unit_base.multi_contoured_set = 1  --main : to avoid being contour multiple times
				if not chatsend_unit_base.net_delay_remove_con then
            		DelayedCalls:Add("WOE_remove_contour" .. number_data, highlight_time, remove_contour)  --in case if we lost network data
					chatsend_unit_base.net_delay_remove_con = true
				end
			end

		end
	end

end)


--If enemy dies, remove his contour
Hooks:Add("HopLibOnUnitDied", "HopLibOnUnitDied_WOE_remove_contour", function(unit, damage_info)
    if not unit:base().is_contoured or unit:base().is_contoured == 1 then  --actually no need to check, but let's be more logical lol
        unit:contour():remove("highlight_character", WOE_Settings_Highlight.Highlight_Radio, 1)
        unit:base().is_contoured = 2
    end
end)