if not WatchOutForEnemies then
	dofile(ModPath .. "Core.lua")
end

if not WatchOutForEnemies.settings.FlashbangMarker.FM_Enable then
	return
end

local FlashbangMarker = WatchOutForEnemies.settings.FlashbangMarker

local QuickFlashGrenade_init = QuickFlashGrenade.init
function QuickFlashGrenade:init(...)
	QuickFlashGrenade_init(self, ...)
	
	local hud = managers.hud and managers.hud:script(PlayerBase.PLAYER_INFO_HUD_FULLSCREEN_PD2)
	if not hud then
		return
	end

	self._flashgrenade_marker = hud.panel:bitmap({
		visible = false,
		color = tweak_data.hud.detected_color,
		blend_mode = "add",
		texture = "guis/textures/pd2/equipment_02",
		texture_rect = {
			32,
			0,
			32,
			32
		}
	})

	self._flashgrenade_marker:set_size(FlashbangMarker.FM_Size, FlashbangMarker.FM_Size)
	self._activate_time = Application:time()
	self._flash_point = 20
	self._flash_speed = 0
end


Hooks:PostHook(QuickFlashGrenade, "update", "update_flashgrenade_marker", function (self, unit, t, dt)
	if not alive(self._flashgrenade_marker) then
		return
	end
	
	local ws = managers.hud._workspace
	local current_camera = managers.viewport:get_current_camera()
	
	if not managers.player:player_unit() or not ws or not current_camera then
		self:_remove_marker()
		return
	end
	
	local screen_pos = ws:world_to_screen(current_camera, self._unit:position())
	
	self._flashgrenade_marker:set_center(mvector3.x(screen_pos), mvector3.y(screen_pos)-10)
	self._marker_flash_speed = t - self._activate_time
	self._flash_speed =self._flash_speed + self._marker_flash_speed * (dt*100)
	local distance = mvector3.distance(managers.player:player_unit():position(), self._unit:position())

	if screen_pos.z > 1 and distance < 2000 then
		self._flashgrenade_marker:set_alpha(1)
	else
		self._flashgrenade_marker:set_alpha(0)
	end
	
	if self._flash_speed >= self._flash_point then
		self._flashgrenade_marker:set_visible(true)
		if self._flash_speed >= self._flash_point + 40 then
			self._flashgrenade_marker:set_visible(false)
			self._flash_point = self._flash_point + 60
		end
	end
end)

function QuickFlashGrenade:_remove_marker()
	if alive(self._flashgrenade_marker) then
		self._flashgrenade_marker:parent():remove(self._flashgrenade_marker)
		self._flashgrenade_marker = nil
	end
end

Hooks:PostHook(QuickFlashGrenade, "make_flash", "make_flash:remove_marker", QuickFlashGrenade._remove_marker)

Hooks:PreHook(QuickFlashGrenade, "on_flashbang_destroyed", "on_flashbang_destroyed:remove_marker", QuickFlashGrenade._remove_marker)
