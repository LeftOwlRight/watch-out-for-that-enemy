if not SC then
    --return
end

function WOE_Exist()
    if WatchOutForEnemies and WOE_Settings then
        return true
    end
    return false
end

if not WOE_Exist() then
    dofile(Mod_Path .. "Core.lua")
end

local WOE_Settings_CFT = WOE_Settings_Chat.Chat_Filter.CF_Tag

local peer = managers.network and managers.network:session() or nil
peer = peer and peer:local_peer() or nil

local camera
local from
local to
local raycast
if managers.player:player_unit() then
    camera = managers.player:player_unit():camera() -- 获取玩家的视角摄像机
end
if camera then
    from = camera:position() -- 获取视角摄像机位置
    to = camera:position() + camera:rotation():y() * 50000 -- 设置摄像机位置看到的地方的最远距离
    raycast = World:raycast("ray", from, to, "slot_mask",
        managers.slot:get_mask("enemies", "bullet_blank_impact_targets")) -- 检查摄像机是否与东西碰撞，若碰撞类型为enemies或者wall/prop，则返回单位
end

-- 可以在这里判断unit的id
if raycast and raycast.unit then

    -- so I can use UnitInfo in Hoplib and check if it is a wall (return nil if wall)
    local unit_info = HopLib:unit_info_manager():get_info(raycast.unit)

    if unit_info then -- or check managers.enemy:is_enemy(unit), but we're using Hoplib anyway

        -- type(raycast.unit) --userdata

        -- set tags of markable enemies
        local available_tag = {}
        if WOE_Settings_CFT.CFT_tank then
            table.insert(available_tag, "tank")
        end
        if WOE_Settings_CFT.CFT_taser then
            table.insert(available_tag, "taser")
        end
        if WOE_Settings_CFT.CFT_medic then
            table.insert(available_tag, "medic")
        end
        if WOE_Settings_CFT.CFT_spooc then
            table.insert(available_tag, "spooc")
        end
        if WOE_Settings_CFT.CFT_shield then
            table.insert(available_tag, "shield")
        end
        if WOE_Settings_CFT.CFT_sniper then
            table.insert(available_tag, "sniper")
        end

        local available_enemy = {'boom', 'autumn', 'spring', 'summer', 'taser_titan', 'omnia_lpf', 'phalanx_vip', 'enforcer_assault',
                                 'medic_summers', 'taser_summers', 'boom_summers', 'fbi_vet', 'vetlod'}

        -- to get Copbase and Copbase.hastag function
        local unit_base = raycast.unit:base() -- or nil
        local unit_tag
        local unit_name_id
        if unit_base then
            unit_tag = unit_base.has_any_tag -- or nil
            unit_base.is_noted = unit_base.is_noted or nil
            unit_name_id = unit_base._tweak_table
            -- log("name_id is "..unit_name_id)  --tank_mini --swat
        end

        -- check if the unit is a special/boss unit.  true or false
        local is_special = unit_info:is_special()
        local is_boss = unit_info:is_boss()
        
        local unit_hash = unit_info:key() -- important, but not for this mod
        local unit_id = unit_info:id()
        local unit_name = unit_info:name() -- get localized unit name straightly

        -- @param table of unit_name_id
        local function is_certain_enemy(enemy)
            for _, checkenemy in ipairs(enemy) do
                if unit_name_id and checkenemy == unit_name_id then
                    return true
                end
            end
            return false
        end

        -- @DelayedCalls to remove contour
        local function remove_contour()
            if not alive(raycast.unit) or not unit_base then
                return
            end
            raycast.unit:contour():remove("highlight_character", WOE_Settings_Highlight.Highlight_Radio, 1)
            unit_base.is_contoured = 1
            unit_base.multi_contoured_set = nil
            unit_base.delay_remove_con = nil
        end

        local function note_recover()
            if not alive(raycast.unit) or not unit_base then
                return
            end
            unit_base.is_noted = nil
            unit_base.delay_note = nil
        end

        local function mark_recover()
            if not alive(raycast.unit) or not unit_base then
                return
            end
            unit_base.is_me_mark = nil
            unit_base.delay_me = nil
        end

        -- unit_name 是用hoplib返回名字，要是没返回就用ene_连接id返回名字
        local unit_name = unit_name or HopLib:name_provider():name_by_unit(raycast.unit) or
                              HopLib:name_provider():name_by_id(raycast.unit:base()._tweak_table) or
                              managers.localization:text('ene_' .. tostring(raycast.unit:base()._tweak_table))

        -- weapon_name 获取到武器id后，转换为string，用gsub把武器id后面的_npc去掉
        local weapon_name = "ERROR"  --can't get
        if raycast.unit.inventory and raycast.unit:inventory() then
            weapon_name = string.gsub(tostring(raycast.unit:inventory():equipped_unit():base()._name_id), '_npc', '', 1)
        end

        -- unit_health 返回血量，*100是为了让它变成整数
        local unit_health = (raycast.unit:character_damage()._health) * 10

        -- 获取单位距离，但是小数点很长，于是用x%1取余得到小数，再用x-x%1的方式去掉多余的小数点
        local unit_distance
        if managers.player:player_unit() then
            unit_distance = mvector3.direction(Vector3(), raycast.unit:movement():m_com(),
                managers.player:player_unit():position()) / 100
        end
        if unit_distance then
            unit_distance = unit_distance - unit_distance % 1
        else
            unit_distance = "ERROR" -- can't get
        end


        --Tells my Cooldown setting : Cooldown
        local copytime_message = {
            key_1 = "cooldown",
            value_1 = WOE_Settings_Chat.Chat_Cooldown,
            key_2 = "unit_id",
            value_2 = unit_id
        }
        LuaNetworking:SendToPeers('WOE_bote_copytime', json.encode(copytime_message))

        --Tells my Highlight setting : Time
        local highlight_time_message = {
            key_1 = "cooldown",
            value_1 = WOE_Settings_Highlight.Highlight_Persist,
            key_2 = "unit_id",
            value_2 = unit_id
        }
        LuaNetworking:SendToPeers('WOE_bote_highlight_time', json.encode(highlight_time_message))


        -- hazard_level copied from hud
        local hazard_level = WOE_getHazardLevel(unit_health, unit_distance)

        -- localization stuff
        local Be_Careful = WOE_Localization("WOE_Be_Careful")
        local Weapon_using = WOE_Localization("WOE_Weapon_using")
        local HP_show = WOE_Localization("WOE_HP_show")
        local distance_me = WOE_Localization("WOE_distance_me")
        local actual_meter = WOE_Localization("WOE_actual_meter")
        local message_sent = ""

        if unit_base.is_noted_by_others then
            message_sent = WOE_Localization("WOE_copy")
        elseif unit_distance then
            message_sent = string.format(
                Be_Careful .. "%s" .. Weapon_using .. "%s" .. HP_show .. "%.f" .. distance_me .. "%.f" .. actual_meter,
                unit_name, weapon_name, unit_health, unit_distance)
        else
            message_sent = string.format(
                Be_Careful .. "%s" .. Weapon_using .. "%s" .. HP_show .. "%.f" .. distance_me .. "%s" .. actual_meter,
                unit_name, weapon_name, unit_health, unit_distance)
        end

        --

        if unit_base and unit_tag then
            if is_special and unit_tag(unit_base, available_tag) or is_boss or is_certain_enemy(available_enemy) then

                --Note unit stuffs
                if not unit_base.is_noted then
                    if WOE_Settings_Chat.Chat_Radio == 3 then

                        --send message
                        if not unit_base.is_me_mark then
                            managers.chat:send_message(1, peer, message_sent)
                            if not unit_base.delay_me then
                                DelayedCalls:Add("WOE_me_mark" .. unit_id, WOE_Settings_Chat.Chat_Cooldown, mark_recover)
                                unit_base.delay_me = true
                            end
                        end
                        if unit_base.is_noted_by_others then
                            unit_base.has_copy = true
                        end

                        --Tell others who I marked
                        local chatsend_message = {
                            key_1 = "unit_id",
                            value_1 = unit_id
                        }
                        LuaNetworking:SendToPeers('WOE_bote_chatsend', json.encode(chatsend_message)) -- NetWork Active Stuff

                    elseif WOE_Settings_Chat.Chat_Radio == 2 then
                        --message only shows to myself
                        managers.chat:receive_message_by_peer(1, managers.network:session():local_peer(), message_sent)
                    end
                    unit_base.is_noted = 1  --the unit has been noted
                    if WOE_Settings_Chat.Chat_Cooldown ~= 0 and not unit_base.delay_note then   --if not never
                        DelayedCalls:Add("WOE_note_cooldown" .. unit_id, WOE_Settings_Chat.Chat_Cooldown, note_recover)  --make unit able to be noted again
                        unit_base.delay_note = true
                    end
                end

                --add contour to unit stuffs
                if not unit_base.multi_contoured_set and not unit_base.is_noted_by_others then

                    --add contour
                    raycast.unit:contour():add("highlight_character", WOE_Settings_Highlight.Highlight_Radio, 1)
                    unit_base.is_me_mark = true

                    --doing logics after adding contour
                    if not unit_base.is_contoured or unit_base.is_contoured == 1 then
                        unit_base.multi_contoured_set = 1  --to avoid being contour multiple times
                        if not unit_base.delay_remove_con then
                            DelayedCalls:Add("WOE_remove_contour" .. unit_id, WOE_Settings_Highlight.Highlight_Persist, remove_contour)  --remove contour
                            unit_base.delay_remove_con = true
                        end
                    end
                end

            end
        end
    end

end


